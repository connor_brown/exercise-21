﻿using System;

namespace exercise_21
{
    class Program
    {
        static void Main(string[] args)
        {
            var counter = 5;
            var i = 0;

            do
            {
                var a = i + 1;
                Console.WriteLine($"this is line number{a}");
                
                
                i++;
            } while (i < counter);
        }
    }
}
